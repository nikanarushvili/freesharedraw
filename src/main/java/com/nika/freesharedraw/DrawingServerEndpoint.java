package com.nika.freesharedraw;

import com.nika.freesharedraw.messaging.DrawingMessage;
import com.nika.freesharedraw.messaging.DrawingMessageDecoder;
import com.nika.freesharedraw.messaging.DrawingMessageEncoder;
import javax.websocket.server.*;
import javax.websocket.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Level;
import java.util.logging.Logger;

@ServerEndpoint(value = "/join/draw/{username}",
        decoders = DrawingMessageDecoder.class,
        encoders = DrawingMessageEncoder.class)

public class DrawingServerEndpoint {

    private Session session;
    private final static Set<DrawingServerEndpoint> DRAWINGSERVERENDPOINTS
            = new CopyOnWriteArraySet<>();
    private final static HashMap<String, String> USERS = new HashMap<>();

    @OnOpen
    public void onOpen(Session session,
            @PathParam("username") String username) throws IOException, EncodeException {
        this.session = session;
        DRAWINGSERVERENDPOINTS.add(this);
        USERS.put(session.getId(), username);
    }

    @OnMessage
    public void onMessage(Session session, DrawingMessage message) {
        if (!message.getRes().equals("ping")) {
            DRAWINGSERVERENDPOINTS.remove(this);
            message.setFrom(USERS.get(session.getId()));
            try {
                broadcast(message);
            } catch (IOException | EncodeException ex) {
            }
            DRAWINGSERVERENDPOINTS.add(this);
        }

    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        USERS.remove(session.getId());
        DRAWINGSERVERENDPOINTS.remove(this);
    }

    @OnError
    public void onError(Session session, Throwable throwable) throws IOException, EncodeException {
        session.close();
    }

    private static void broadcast(DrawingMessage message) throws IOException, EncodeException {
        DRAWINGSERVERENDPOINTS.forEach((DrawingServerEndpoint endpoint) -> {
            synchronized(endpoint){
                try {
                    endpoint.session.getBasicRemote().sendObject(message);
                } catch (IOException | EncodeException ex) {
                    Logger.getLogger(DrawingServerEndpoint.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
