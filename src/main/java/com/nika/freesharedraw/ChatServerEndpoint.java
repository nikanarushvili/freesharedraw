/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nika.freesharedraw;

import com.nika.freesharedraw.messaging.ChatMessage;
import com.nika.freesharedraw.messaging.ChatMessageDecoder;
import com.nika.freesharedraw.messaging.ChatMessageEncoder;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Nika.Narushvili
 */
@ServerEndpoint(value = "/join/chat/{username}",
        decoders = ChatMessageDecoder.class,
        encoders = ChatMessageEncoder.class)
public class ChatServerEndpoint {

    private Session session;
    private final static Set<ChatServerEndpoint> CHATSERVERENDPOINTS
            = new CopyOnWriteArraySet<>();
    private final static HashMap<String, String> USERS = new HashMap<>();
    private static LocalDateTime prevAccess;

    @OnOpen
    public void onOpen(Session session,
            @PathParam("username") String username) throws IOException, EncodeException {
        if (!USERS.values().contains(username)) {
            this.session = session;
            CHATSERVERENDPOINTS.add(this);
            ChatMessage message = new ChatMessage();
            message.setFrom(username);
            message.setContent(username + " connected!\n");
            USERS.put(session.getId(), username);
            broadcast(message);
            prevAccess = LocalDateTime.now();
        } else {
            session.close();
        }
    }

    @OnMessage
    public void onMessage(Session session, ChatMessage message) throws IOException, EncodeException {
        if (message.getContent().equals("ping")) {
            return;
        }
        LocalDateTime nowAccess = LocalDateTime.now();
        if (prevAccess.isBefore(nowAccess.minusSeconds(5))) {
            if (message.getContent().equals("allusers")) {
                message.setFrom("server");
                StringBuilder usersSB = new StringBuilder("Connected users : ");
                USERS.values().forEach((u) -> {
                    usersSB.append(u);
                    usersSB.append(", ");
                });
                message.setContent(usersSB.toString().substring(0, usersSB.toString().length() - 2));
                this.session.getBasicRemote().sendObject(message);
            } else {
                broadcast(message);
            }
            prevAccess = nowAccess;
        } else {
            message.setFrom("server");
            message.setContent("sending too many requests at once");
            session.getBasicRemote().sendObject(message);
        }
    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        String username = USERS.get(session.getId());
        new Runnable() {
            @Override
            public void run() {
                USERS.remove(session.getId());
            }
        }.run();
        CHATSERVERENDPOINTS.remove(this);
        new Runnable() {
            @Override
            public void run() {
                ChatMessage message = new ChatMessage();
                message.setFrom(username);
                message.setContent(username + " disconnected.\n");
                broadcast(message);
            }
        }.run();
    }

    @OnError
    public void onError(Session session, Throwable throwable) throws IOException, EncodeException {
        session.close();
    }

    private static void broadcast(ChatMessage message) {
        CHATSERVERENDPOINTS.forEach((ChatServerEndpoint endpoint) -> {
            synchronized(endpoint){
                try {
                    endpoint.session.getBasicRemote().sendObject(message);
                } catch (IOException | EncodeException ex) {
                    Logger.getLogger(ChatServerEndpoint.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
