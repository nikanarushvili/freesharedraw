package com.nika.freesharedraw.messaging;



import javax.websocket.*;

import com.google.gson.Gson;

public class DrawingMessageEncoder implements Encoder.Text<DrawingMessage>{

	private static final Gson GSON = new Gson();
	
	

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String encode(DrawingMessage message) throws EncodeException {
		return GSON.toJson(message);	
	}

}
