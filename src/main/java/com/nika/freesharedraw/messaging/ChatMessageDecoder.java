/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nika.freesharedraw.messaging;

import com.google.gson.Gson;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author Nika.Narushvili
 */
public class ChatMessageDecoder implements Decoder.Text<ChatMessage>{
    private static final Gson GSON = new Gson();
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ChatMessage decode(String message) throws DecodeException {
		// TODO Auto-generated method stub
		return GSON.fromJson(message, ChatMessage.class);
	}

	@Override
	public boolean willDecode(String s) {
		return (s != null);
	}
}
