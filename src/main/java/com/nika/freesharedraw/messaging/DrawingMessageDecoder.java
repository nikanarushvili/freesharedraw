package com.nika.freesharedraw.messaging;


import javax.websocket.*;

import com.google.gson.Gson;
public class DrawingMessageDecoder implements Decoder.Text<DrawingMessage> {
	private static final Gson GSON = new Gson();
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DrawingMessage decode(String message) throws DecodeException {
		// TODO Auto-generated method stub
		return GSON.fromJson(message, DrawingMessage.class);
	}

	@Override
	public boolean willDecode(String s) {
		return (s != null);
	}
	
	
}
