//canvas stuff
var canvas, ctx, flag = false,
        prevX = 0,
        currX = 0,
        prevY = 0,
        currY = 0,
        dot_flag = false;

var x = "black",
        y = 2;

var prevBorderedColor;
function init() {
    document.getElementById("chatarea").innerHTML = "                !!!CHAT!!! \n\n---------------------------------------\n\n";
    canvas = document.getElementById("can");
    ctx = canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;
    document.getElementById("black").style.border = "2px solid purple";
    prevBorderedColor = "black";
    canvas.addEventListener("mousemove", function (e) {
        findxy("move", e);
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        findxy("down", e);
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        findxy("up", e);
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        findxy("out", e);
    }, false);
}

function color(obj) {
    var colorTextHolder = document.getElementById("currentColor");
    x = obj.id;
    if (x === "white") {
        y = 14;
        document.getElementById("can").style.cursor = "url('./resources/eraser.png'), auto";
    } else {
        y = 2;
        document.getElementById("can").style.cursor = "url('./resources/paintbrush.png'), auto";
    }
    colorTextHolder.innerHTML = "Current color - " + x;
    document.getElementById(x).style.border = "2px solid purple";
    document.getElementById(prevBorderedColor).style.border = "2px dotted purple";
    prevBorderedColor = x;
}


function erase() {
    ctx.clearRect(0, 0, w, h);
    sendEraseMsg();
}


var sentReqs = 0;
function findxy(res, e) {
    if (res === "down") {
        prevX = currX;
        prevY = currY;
        currX = e.clientX - canvas.offsetLeft;
        currY = e.clientY - canvas.offsetTop;
        flag = true;
    }
    if (res === "up" || res === "out") {
        flag = false;
    }
    if (res === "move") {
        if (flag) {
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            respDraw(prevX, prevY, currX, currY, x, y);
            send(username, prevX, prevY, currX, currY, x, res, y);
        }
    }

}


//websocket stuff

function checkTimeOut(sendDate) {
    if (sendDate > timeoutDate) {
        timeoutDate = sendDate + 5000;
        return true;
    }
    return false;
}
var drawWs;
var username;
var usersNum = 0;
var timeoutDate = -1;
var chatWs;
function connect() {
    username = document.getElementById("username").value;
    drawWs = new WebSocket("ws://" + window.location.host + window.location.pathname + "join/draw/" + username);
    drawWs.onmessage = function (event) {
        var message = JSON.parse(event.data);
        if (message.res === "erase") {
            ctx.clearRect(0, 0, w, h);
        } else {
            respDraw(message.prevX, message.prevY, message.currX, message.currY, message.color, message.width);
        }
    };
    chatWs = new WebSocket("ws://" + window.location.host + window.location.pathname + "join/chat/" + username);
    chatWs.onmessage = function (event) {
        var message = JSON.parse(event.data);
        if (message.from === "server") {
            console.log(message.content);
        } else {
            document.getElementById("chatarea").innerHTML += message.content;
        }
    };
    document.getElementById("username").disabled = true;
    document.getElementById("connectBtn").disabled = true;
}
var intervalID = window.setInterval(pingServer, 15000);

function getAllConnectedUsers() {
    if (typeof chatWs !== 'undefined') {
        var json = JSON.stringify({
            "from": username,
            "content": "allusers"
        });
        chatWs.send(json);
    }
}


function pingServer() {
    if (typeof chatWs !== 'undefined' && typeof drawWs !== 'undefined') {
        var json = JSON.stringify({
            "from": username,
            "content": "ping"
        });
        chatWs.send(json);
        json = JSON.stringify({
            "from": username,
            "prevX": null,
            "prevY": null,
            "currX": null,
            "currY": null,
            "color": null,
            "res": "ping",
            "width": null
        });
        drawWs.send(json);
    }
}

function sendChatMsg() {
    if (typeof chatWs !== 'undefined' && (chatWs.readyState !== chatWs.CLOSED && chatWs.readyState !== chatWs.CLOSING))
    {
        var chatMsg = document.getElementById("chatMsgText").value;
        if (chatMsg.length >= 1) {
            if (checkTimeOut(new Date().getTime())) {
                var json = JSON.stringify({
                    "from": username,
                    "content": username + ": " + document.getElementById("chatMsgText").value + "\n"
                });
                chatWs.send(json);
            } else {
                document.getElementById("chatarea").innerHTML += "!!!You can send messages every 5 seconds! Time left : " + Math.floor((timeoutDate - new Date().getTime()) / 1000) + " seconds!!!\n";
            }
        } else {
            document.getElementById("chatarea").innerHTML += "!!!type at least 1 symbol!!!\n";
        }
    } else {
        document.getElementById("chatarea").innerHTML += "Connection error!\n";
    }
}
function disconnect() {
    drawWs.close();
    chatWs.close();
    document.getElementById("chatarea").innerHTML += username + " disconnected.\n";
}

function respDraw(respPrevX, respPrevY, respCurrX, respCurrY, respColor, respLineWidth) {
    ctx.beginPath();
    ctx.moveTo(respPrevX, respPrevY);
    ctx.lineTo(respCurrX, respCurrY);
    ctx.strokeStyle = respColor;
    ctx.lineWidth = respLineWidth;
    ctx.stroke();
    ctx.closePath();
}

function clearChat() {
    document.getElementById("chatarea").innerHTML = "                !!!CHAT!!! \n\n---------------------------------------\n\n";
}
function send(respFrom, respPrevX, respPrevY, respCurrX, respCurrY, respColor, respRes, respWidth) {
    if (typeof drawWs !== 'undefined') {
        var json = JSON.stringify({
            "from": respFrom,
            "prevX": respPrevX,
            "prevY": respPrevY,
            "currX": respCurrX,
            "currY": respCurrY,
            "color": respColor,
            "res": respRes,
            "width": respWidth
        });
        drawWs.send(json);
    }
}

function sendEraseMsg() {
    if (typeof drawWs !== 'undefined') {
        send(null, null, null, null, null, null, "erase");
    }
}

function resetMouse() {
    flag = false;
    prevX = 0;
    currX = 0;
    prevY = 0;
    currY = 0;
    dot_flag = false;
}
